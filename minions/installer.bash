cp ./SaltRepo.repo /etc/yum.repos.d/SaltStack.repo
yum update -y
yum install -y salt-minion salt-ssh vim
systemctl enable salt-minion.service --now
systemctl enable firewalld --now
sed 's/#master:\ salt/master:\ saltmaster/g' --in-place=.bak /etc/salt/minion
