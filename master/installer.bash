cp ./SaltRepo.repo /etc/yum.repos.d/SaltStack.repo
cp -vr ./salt /srv/
yum update -y
yum install -y salt-master salt-ssh vim
systemctl enable salt-master.service --now
systemctl enable firewalld --now
firewall-cmd --permanent --zone=public --add-port=4505-4506/tcp
firewall-cmd --reload
curl "https://git.grml.org/?p=grml-etc-core.git;a=blob_plain;f=etc/zsh/zshrc" -o .zshrc
chsh -s /bin/zsh root
