/home/spirits/.ssh/:
  file.directory:
    - user: spirits
    - group: spirits
    - mode: 700

/home/spirits/.ssh/authorized_keys:
  file.managed:
    - source: salt://files/ssh_keys/spirits_id_rsa.pub
    - user: spirits
    - group: spirits
    - mode: 600
