# Top file
base:
  '*':
    - schedule
  'os:CentOS':
    - match: grain
    - sshkeys
    - software
    - sshd_config
    - firewall
    - users
    - shell_config
  'os:Debian':
    - match: grain
    - sshkeys
    - sshd_config
    - users
    - shell_config
#  'os:Debian':
#    - match: grain
#    - sshkeys
#    - sshd_config
#    - software
#  'os:Fedora':
#    - match: grain
#    - sshkeys
#    - software
#    - sshd_config
##  'os:Arch Linux'
##    - match: grain
##    - sshkeys
##    - software
##    - sshd_config
##  'os:FreeBSD':
##    - match: grain
##    - bsdsshkeys
##  'z800':
##    - virt
#
