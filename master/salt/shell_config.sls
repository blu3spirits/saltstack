zsh:
  pkg.installed
/root/.zshrc:
  file.managed:
  - source: https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
  - source_hash: 9a3bbcf651c86ec5533646ad1efee958
  - user: root
  - group: root
  - mode: 644
/home/admin/.zshrc:
  file.managed:
  - source: https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
  - source_hash: 9a3bbcf651c86ec5533646ad1efee958
  - user: admin
  - group: admin
  - mode: 644
