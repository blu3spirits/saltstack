common_packages:
  pkg.installed:
    - pkgs:
      - tmux
      - tcpdump
      - htop
      - wget
      - zsh
      - firewalld
